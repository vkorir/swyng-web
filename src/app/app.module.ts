import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BannerComponent } from './banner/banner.component';
import { CarpoolComponent } from './carpool/carpool.component';
import { FeaturesComponent } from './features/features.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { AppstoreComponent } from './appstore/appstore.component';
import { AboutComponent } from './about/about.component';
import { TeamComponent } from './team/team.component';
import { DestinationComponent } from './destination/destination.component';
import { FooterComponent } from './footer/footer.component';
import { TopBarComponent } from './top-bar/top-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    BannerComponent,
    CarpoolComponent,
    FeaturesComponent,
    TestimonialsComponent,
    AppstoreComponent,
    AboutComponent,
    TeamComponent,
    DestinationComponent,
    FooterComponent,
    TopBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
